<?php
namespace Staging;

class GitHandler {

    private static $git_dir = null;

    public function __construct($git_dir) {

        $this->git_dir = $git_dir;

    }

    public function loadRepositoryConfig($repository) {
        return json_decode(file_get_contents($this->git_dir . "/" . $repository . "/config.json"), true);
    }

    public function listBranches($repository) {
        $config = $this->loadRepositoryConfig($repository);
        $git_user = $config["git_user"];
        exec("git ls-remote git@bitbucket.org:$git_user/$repository.git", $res);
        $branches = array();

        foreach($res as $br) {
            $parts = explode("\t", $br);
            //array_shift($parts);
            $p2 = explode("/", $parts[1]);
            $name = $p2[sizeof($p2)-1];
            $hash = substr($parts[0],0,7);

            if($name !== "HEAD") {
                $branches[$name] = array(
                    "remote" => $hash,
                    "local" => null
                );
            }
        }



        foreach (new \DirectoryIterator( $this->git_dir . "/" . $repository ) as $fileInfo) {
            if($fileInfo->isDot() || !$fileInfo->isDir()) continue;
            $branch = $fileInfo->getFilename();

            chdir($this->git_dir   . "/$repository/$branch");
            $hash = exec("git rev-parse --short HEAD", $res);

            $branches[$branch]["local"] = $hash;
        }

        return $branches;
    }

    public function listRepositories() {
        $repositories = array();
        foreach (new \DirectoryIterator( $this->git_dir ) as $fileInfo) {
            if($fileInfo->isDot() || !$fileInfo->isDir()) continue;

            $repository = $fileInfo->getFilename();

            $count = exec("ls -l " . $this->git_dir . "/$repository | wc -l") - 2;
            $repositories[] = array(
                "name" => $repository,
                "branch_count" => $count
            );
        }

        return $repositories;
    }

    public function update( $repositories = array(), $branches = array() ) {

        $return = array();

        foreach (new \DirectoryIterator( $this->git_dir ) as $fileInfo) {
            if($fileInfo->isDot() || !$fileInfo->isDir()) continue;
            $repository = $fileInfo->getFilename();

            if( !empty($repositories) && !in_array($repository, $repositories) ) continue;

            foreach (new \DirectoryIterator( $this->git_dir . "/" . $repository ) as $br) {
                if( $br->isDot() || !$br->isDir() ) continue;
                $branch = $br->getFilename();

                if( !empty($branches) && !in_array($branch, $branches) ) continue;

                //echo  $repository . "/". $branch. "<br>\n";

                chdir($this->git_dir . "/$repository/$branch");

                exec(GIT . " reset --hard HEAD");
                $hash_old = exec(GIT . " rev-parse --short HEAD");
                exec(GIT . " pull origin $branch");
                $hash_new = exec(GIT . " rev-parse --short HEAD");

                if(empty($return[$repository])) {
                    $return[$repository] = array();
                }

                $return[$repository][$branch] = array(
                    "updated" => false,
                    "composer" => false
                );

                if( $hash_new !== $hash_old ) {
                    //echo " --> updated ($hash_new)<br>\n";
                    $return[$repository][$branch]["updated"] = true;

                    if(file_exists("composer.json")) {
                        exec(COMPOSER . " update");
                        //echo " --> composer updated<br />\n";
                        $return[$repository][$branch]["composer"] = true;
                    }
                }
            }
        }

        return $return;
    }
}