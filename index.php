<?php

error_reporting(E_ALL);
ini_set("display_errors", true);

define("BASE", __DIR__);
define("COMPOSER", "php /home/dominik/composer.phar");
define("GIT", "git");

require 'vendor/autoload.php';

$gh = new Staging\GitHandler(BASE . "/git");

$app = new Slim\Slim(array(
    "debug" => true,
    "templates.path" => BASE . "/templates"

));

$app->get("/", function () use ($app, $gh) {

    $app->render('repositories.php', array('repositories' => $gh->listRepositories()));

});

$app->get("/update", function () use ($gh, $app) {

    $req = $app->request();
    $get = $req->get();
    $repositories = !empty($get["repositories"]) ? explode(",", $get["repositories"]) : array();
    $branches =  !empty($get["branches"]) ? explode("," ,$get["branches"]) : array();

    $ret = $gh->update($repositories, $branches);

    echo json_encode($ret);

});

$app->get("/:repository", function ($repository) use ($gh, $app) {

    $app->render('branches.php', array(
        "repository_name" => $repository,
        "branches" => $gh->listBranches($repository)
    ));

});






$app->run();