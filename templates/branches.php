<a href="/">back</a>
<table width="20%">
    <tr>
        <th>Name</th>
        <th>Local</th>
        <th>Remote</th>
    </tr>
    <?php foreach($branches as $name => $hashes) :?>
        <tr>
            <td><?= $name ?></td>
            <td><?= $hashes["local"] ?></td>
            <td><?= $hashes["remote"] ?></td>
            <td>
                <?php if($hashes["local"] === null): ?>
                    <a href="#" class="clone" data-repository="<?= $repository_name ?>" data-branch="<?= $name ?>">clone</a>
                <?php elseif($hashes["local"] !== $hashes["remote"]): ?>
                    <a href="#" class="update" data-repository="<?= $repository_name ?>" data-branch="<?= $name ?>">update</a>
                <?php endif; ?>
            </td>
        </tr>
    <?php endforeach; ?>

</table>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script type="text/javascript">
    $(function() {
        $(document).on("click", "a.update", function() {
            var repository = $(this).data("repository"),
                branch = $(this).data("branch"),
                that = $(this);
            that.text("updating...");

            $.getJSON("/update", {
                repositories: repository,
                branches: branch
            }, function(d) {
                that.text("done");
                console.log(d);
            })
        });
    });
</script>