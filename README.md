# VHOST Entry

    <VirtualHost *:80>
        DocumentRoot /var/www/staging/git

        ServerAlias *.staging.creativebrains.net

        VirtualDocumentRoot /var/www/staging/git/%-4/%-5+

        <Directory /var/www/staging/git >
            Options FollowSymLinks
            AllowOverride All
        </Directory>

        Alias /base /var/www/staging/
        ErrorDocument 404 /base/error.php

        ErrorLog /var/log/apache2/staging_creativebrains_net.error.log
        CustomLog /var/log/apache2/staging_creativebrains_net.access.log combined
    </VirtualHost>
    <VirtualHost *:80>
        DocumentRoot /var/www/staging
        ServerName staging.creativebrains.net

        <Directory /var/www/staging >
            Options FollowSymLinks
            AllowOverride All
        </Directory>


        ErrorLog /var/log/apache2/staging_creativebrains_net.error.log
        CustomLog /var/log/apache2/staging_creativebrains_net.access.log combined
    </VirtualHost>


hf&gl
