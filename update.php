<?php

error_reporting(E_ALL);
ini_set("display_errors", true);

define("TIME_START", microtime(true));
define("BASE", __DIR__);

define("COMPOSER", "php /home/dominik/composer.phar");
define("GIT", "git");

$repositories = !empty($_GET["repositories"]) ? explode(",", $_GET["repositories"]) : array();
$branches = !empty($_GET["branches"]) ? explode("," ,$_GET["branches"]) : array();

$gh = new GitHandler( BASE . "/git" );
$gh->update($repositories, $branches);

echo "<br/>\nruntime: " . (microtime(true) - TIME_START);




